package br.com.sawp.so.proj2

import util.Random
import br.com.sawp.so.proj2.facilities.Bathroom
import br.com.sawp.so.proj2.people._
import br.com.sawp.so.proj2.Properties.properties

/**
 * Author: Pedro Garcia <sawp@sawp.com.br>
 * http://www.sawp.com.br/
 * Date: 3/11/14
 * Time: 10:32 AM
 */


object Simulation {
  def main(args: Array[String]) {
    val numberOfBoxes = properties("numberOfBoxes")
    val numberOfMen = properties("numberOfMen")
    val numberOfWomen = properties("numberOfWomen")
    val bathroom = new Bathroom(numberOfBoxes)

    val men = (1 to numberOfMen).map {
      i => new Man(i, bathroom)
    }

    val women = (1 to numberOfWomen).map {
      i => new Woman(i, bathroom)
    }

    val people = Random.shuffle(men ++ women)
    //val people = men ++ women
    people.foreach {
      person => person.start()
    }
  }
}