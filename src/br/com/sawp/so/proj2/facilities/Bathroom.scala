package br.com.sawp.so.proj2.facilities

import java.util.concurrent.Semaphore
import br.com.sawp.so.proj2.people._
import br.com.sawp.so.proj2.ui.UserInterface
import java.lang.management.ManagementFactory

/**
 * Author: Pedro Garcia <sawp@sawp.com.br>
 * http://www.sawp.com.br/
 * Date: 3/11/14
 * Time: 2:09 PM
 */
class Bathroom(numberOfBoxes: Int) {
  val menSwitch = new GenderRestrictionController()
  val womenSwitch = new GenderRestrictionController()
  val menMultiplex = new Semaphore(numberOfBoxes)
  val womenMultiplex = new Semaphore(numberOfBoxes)
  val flag = new Semaphore(1)
  val emptyBathroomSemaphore = new Semaphore(1)


  def enter(person: Person) = person match {
    case person: Woman => womanEnter()
    case person: Man => manEnter()
  }

  def exit(person: Person) = person match {
    case person: Woman => womanExit()
    case person: Man => manExit()
  }

  private def womanEnter() {
    flag.acquire()
    womenSwitch.lock(emptyBathroomSemaphore)
    flag.release()
    womenMultiplex.acquire()
    UserInterface.arrivesWoman()
  }

  private def womanExit() {
    UserInterface.leavesWoman()
    womenMultiplex.release()
    womenSwitch.unlock(emptyBathroomSemaphore)
  }

  private def manEnter() {
    flag.acquire()
    menSwitch.lock(emptyBathroomSemaphore)
    flag.release()
    menMultiplex.acquire()
    UserInterface.arrivesMan()
  }

  private def manExit() {
    UserInterface.leavesMan()
    menMultiplex.release()
    menSwitch.unlock(emptyBathroomSemaphore)
  }
}
