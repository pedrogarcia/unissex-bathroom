package br.com.sawp.so.proj2.facilities

import java.util.concurrent.Semaphore
import br.com.sawp.so.proj2.ui.UserInterface

/**
 * Author: Pedro Garcia <sawp@sawp.com.br>
 * http://www.sawp.com.br/
 * Date: 3/11/14
 * Time: 2:08 PM
 */
class GenderRestrictionController {
  val mutex = new Semaphore(1)
  var counter = 0

  def lock(semafor: Semaphore) {
    mutex.acquire()
    counter += 1
    if (counter == 1) {
      semafor.acquire()
    }
    mutex.release()
  }

  def unlock(semafor: Semaphore) {
    mutex.acquire()
    counter -= 1
    if (counter == 0) {
      semafor.release()
    }
    mutex.release()
  }
}
