package br.com.sawp.so.proj2

import scala.io.Source
import scala.util.parsing.json.JSON

/**
 * Author: Pedro Garcia <sawp@sawp.com.br>
 * http://www.sawp.com.br/
 * Date: 3/12/14
 * Time: 3:04 PM
 */
object Properties {
  def properties = _properties

  private val _properties: Map[String, Int] = {
    val propFileStream = getClass.getResourceAsStream("/properties.json")
    val jsonFromFile = Source.fromInputStream(propFileStream).getLines.mkString
    JSON.parseFull(jsonFromFile) match {
      case Some(e) =>
        e.asInstanceOf[Map[String, Double]] map {
          case (k, v) => (k, v.toInt)
        }
      case None =>
        throw new RuntimeException("Properties.json does not exists or is an invalid JSON.")
    }
  }
}
