package br.com.sawp.so.proj2.people

import br.com.sawp.so.proj2.facilities.Bathroom
import br.com.sawp.so.proj2.ui.UserInterface


abstract class Person(id: Int, typeo: String, bathroom: Bathroom) extends Thread {
  this.setName(typeo + "-" + id)

  def enter() {
    bathroom.enter(this)
    UserInterface.showInTerminal(typeo + " " + id + " enters in the bathroom.")
  }

  def exit() {
    UserInterface.showInTerminal(typeo + " " + id + " left the bathroom.")
    bathroom.exit(this)
  }

  override
  def run() {
      enter()
      Thread.sleep(Person.getRandInt)
      exit()
  }
}


protected object Person {
  private val rnd = new scala.util.Random
  private val range = 1000 to 2000
  def getRandInt = range(rnd.nextInt(range length))
}


class Man(id: Int, bathroom: Bathroom) extends Person(id, "Man", bathroom)

class Woman(id: Int, bathroom: Bathroom) extends Person(id, "Woman", bathroom)

