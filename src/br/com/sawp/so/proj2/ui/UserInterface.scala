package br.com.sawp.so.proj2.ui

import scala.sys.process._
import br.com.sawp.so.proj2.Properties.properties
import java.lang.management.ManagementFactory

/**
 * Author: Pedro Garcia <sawp@sawp.com.br>
 * http://www.sawp.com.br/
 * Date: 3/11/14
 * Time: 4:03 PM
 */
object UserInterface {
  private var manInBathroom = 0
  private var womanInBathroom = 0
  private var manWaiting = 0
  private var womanWaiting = 0
  private var totalOfPeople = 0

  def showInTerminal(msg: String) {
    this.synchronized {
      refreshThreadInfo
      print("clear || cls".!!)
      println("Men in Bathroom: " + manInBathroom)
      println("Women in Bathroom:" + womanInBathroom)
      println("Men waiting: " + manWaiting)
      println("Women waiting: " + womanWaiting)
      println("Idle boxes: " + (properties("numberOfBoxes") - (manInBathroom + womanInBathroom)))
      println("[>> " + msg)
      Thread.sleep(500)
    }
  }

  def arrivesMan() {
    this.synchronized {
      manInBathroom += 1
    }
  }

  def leavesMan() {
    this.synchronized {
      manInBathroom -= 1
    }
  }

  def arrivesWoman() {
    this.synchronized {
      womanInBathroom += 1
    }
  }

  def leavesWoman() {
    this.synchronized {
      womanInBathroom -= 1
    }
  }

  def refreshThreadInfo() {
    val infos = ManagementFactory.getThreadMXBean().dumpAllThreads(true, true)
    val womenThreads = infos.filter {
      thread => thread.getThreadName contains "Woman-"
    }
    val menThreads = infos.filter {
      thread => thread.getThreadName contains "Man-"
    }
    totalOfPeople = (womenThreads ++ menThreads).length
    /*
    manInBathroom = menThreads.filter {
      m => m.getThreadState() == Thread.State.RUNNABLE
    }.length
    womanInBathroom = womenThreads.filter {
      w => w.getThreadState() == Thread.State.RUNNABLE
    }.length
    */

    manWaiting = menThreads.filter {
      m => m.getThreadState != Thread.State.RUNNABLE
    }.length

    womanWaiting = womenThreads.filter {
      w => w.getThreadState != Thread.State.RUNNABLE
    }.length
  }
}
